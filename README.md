# Android Shopping Cart

This is an online shopping application which allows users to browse for products in various kinds of categories and purchase them. It also provides options to select delivery types (store pickup or delivery).

### Build requirements

Make sure you have installed the following software before proceed with the installation.
- Java Development Kit 1.8
- Android studio 2.3.3

## AVD configuration to support locally hosted server 

Inorder to let the application access the api server hosted locally please add 
host mapping in your AVD for the *shopping.api.localhost* domain,

1) run the avd as a writable system, (inorder to find the avd name first run the list avd command)
go to **<sdk-location>/emulator** (if you have the emulator installed or to **<sdk-location>/tools**)
```
emulator -list-avds
emulator -writable-system -avd <the relevant avd name>
```

2) once the avd starts, copy the */etc/hosts* file to your local pc with following command with adb. 
(adb is located in **<sdk-location>/platform-tools**)
```
./adb pull /system/etc/hosts <path to the location to save the hosts file>
```

3) change and save the hosts file which was taken from avd and map the local ip 
(or the emulators localhost ip (10.0.2.2) to the relevant domain)
```
127.0.0.1		    localhost
10.0.2.2    shopping.api.localhost
```

4) login to the avd using shell and re-mount the **system** with read/write privileges (use *exit* command to exit from the avd terminal)
```
// login to avd and get root privileges 
adb shell
su

// to get the current system mount details
// eg : result -> /dev/block/vda /system ext4 ro,relatime,data=ordered 0 0
cat /proc/mounts | grep system

// to re-mount as rw, please update the device-path and mount-point according to results of the cat
// eg : mount -o remount,rw -t ext4 /dev/block/vda /system
mount -o remount,rw -t <type> <device-path> <mount-point>
```

5) once the system is mounted as rw copy the changed *hosts* file back to the avd and re-mount system as read only
```
adb push <path-to-edited-hosts-file>/hosts /system/etc

// login to avd
adb shell
su
// eg : mount -o remount,ro -t ext4 /dev/block/vda /system
mount -o remount,ro -t <type> <device-path> <mount-point>
```

6) once above process is done shutdown the emulator and run the apk via Android Studio (run the emulator in normal way)
***Note : if you are using multiple emulators for testing please do the same for all the emulators.