package com.hsi.shoppingcart.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.domain.ItemDetails;
import com.hsi.shoppingcart.domain.ShoppingCartRealmModule;
import com.hsi.shoppingcart.services.ShoppingCartService;
import com.hsi.shoppingcart.services.listeners.MainNavigationDrawerItemSelectedListener;
import com.hsi.shoppingcart.util.SystemUtil;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ItemDetailsActivity extends AppCompatActivity {

    private static final int REQ_START_STANDALONE_PLAYER = 1;
    private static final int REQ_RESOLVE_SERVICE_MISSING = 2;
    private static final String API_KEY = "AIzaSyBh0oIxUpsG4Z35VNNHhB-007WUuMOkdkY";
    CarouselView carouselView;
    // Array of images to be inserted to the image carousel
    int[] carouselImages = new int[]{R.drawable.category_televisions, R.drawable.category_camera, R.drawable.category_womens_clothes};
    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(carouselImages[position]);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Item details");
        setContentView(R.layout.activity_item_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.item_details_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.items_list_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.item_list_nav_view);
        MainNavigationDrawerItemSelectedListener drawerItemSelectedListener =
                new MainNavigationDrawerItemSelectedListener(getApplicationContext(), this);
        navigationView.setNavigationItemSelectedListener(drawerItemSelectedListener);
        SystemUtil.setNavigationHeaderAndItemAccordingToUser(navigationView, this);

        Bundle extras = getIntent().getExtras();
        TextView txtItemName = (TextView) findViewById(R.id.txtItemName);
        TextView txtItemDescription = (TextView) findViewById(R.id.txtItemDescription);
        TextView txtItemPrice = (TextView) findViewById(R.id.txtItemPrice);
        if (extras == null || extras.isEmpty()) {
            txtItemName.setText("Item name");
            txtItemDescription.setText("Lorem Ipsum is simply dummy text of the printing and typesetting" +
                    " industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s," +
                    " when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
            txtItemPrice.setText("Price: $10.00");
        } else {
            final ItemDetails itemDetails = (ItemDetails) extras.getSerializable("selectedItem");
            txtItemName.setText(itemDetails.getName());
            setTitle(itemDetails.getName());
            txtItemDescription.setText(itemDetails.getDescription());
            txtItemPrice.setText("Price: " + itemDetails.getPrice());
            if (itemDetails.isVideoItem()) {
                Button previewButton = (Button) findViewById(R.id.video_preview_button);
                previewButton.setVisibility(View.VISIBLE);
                previewButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = itemDetails.getVideoUrl();
                        if (itemDetails.isYoutubeVideo()) {
                            playVideoInYoutube(url);
                        } else {
                            Intent intent = new Intent(ItemDetailsActivity.this, VideoPlayActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("video_url", url);
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            ItemDetailsActivity.this.startActivity(intent);
                        }
                    }
                });
            }
        }
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(carouselImages.length);
        carouselView.setImageListener(imageListener);

        FloatingActionButton cartFloatingButton = (FloatingActionButton) findViewById(R.id.cartfloatingButton);
        cartFloatingButton.setOnClickListener(new FloatingButtonClickListener(ItemDetailsActivity.this));
        //TODO: Remove deleteRealmIfMigrationNeeded() before a release
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).deleteRealmIfMigrationNeeded().setModules(new ShoppingCartRealmModule()).build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private void playVideoInYoutube(String url) {
        Intent intent = YouTubeStandalonePlayer.createVideoIntent(ItemDetailsActivity.this,
                API_KEY, url, 0, true, false);
        if (intent != null) {
            if (canResolveIntent(intent)) {
                startActivityForResult(intent, REQ_START_STANDALONE_PLAYER);
            } else {
                YouTubeInitializationResult.SERVICE_MISSING
                        .getErrorDialog(ItemDetailsActivity.this, REQ_RESOLVE_SERVICE_MISSING).show();
            }
        }
    }

    private boolean canResolveIntent(Intent intent) {
        List<ResolveInfo> resolveInfo = getPackageManager().queryIntentActivities(intent, 0);
        return resolveInfo != null && !resolveInfo.isEmpty();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_START_STANDALONE_PLAYER && resultCode != RESULT_OK) {
            YouTubeInitializationResult errorReason =
                    YouTubeStandalonePlayer.getReturnedInitializationResult(data);
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, 0).show();
            } else {
                String errorMessage = errorReason.toString();
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        }
    }

    class FloatingButtonClickListener implements View.OnClickListener {
        final ItemDetailsActivity currentActivity;

        FloatingButtonClickListener(ItemDetailsActivity currentActivity) {
            this.currentActivity = currentActivity;
        }

        @Override
        public void onClick(View v) {

            final Dialog dialog = new Dialog(currentActivity);
            dialog.setContentView(R.layout.cart_add_dialog);
            dialog.setTitle("Add to cart");
            final EditText txtQuantity = (EditText) dialog.findViewById(R.id.txtQuantity);
            Button btnSave = (Button) dialog.findViewById(R.id.btnAddToCart);
            Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
            dialog.show();

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (txtQuantity.getText().length() == 0 || Integer.parseInt(txtQuantity.getText().toString()) <= 0) {
                        txtQuantity.setError("Invalid quantity.");
                    } else {
                        ShoppingCartService shoppingCartService = new ShoppingCartService(getApplicationContext());
                        shoppingCartService.addToCart("p0001", 1, 10);
                        dialog.dismiss();
                        Toast toast = Toast.makeText(currentActivity, "Item added to the cart successfully.", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            });
        }
    }
}
