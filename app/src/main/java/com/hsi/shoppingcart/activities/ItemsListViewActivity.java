package com.hsi.shoppingcart.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.adapters.RecycleViewAdapter;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.services.DataLoaderService;
import com.hsi.shoppingcart.services.listeners.ItemListCustomOnScrollListener;
import com.hsi.shoppingcart.services.listeners.MainNavigationDrawerItemSelectedListener;
import com.hsi.shoppingcart.util.SystemUtil;

import java.util.List;

public class ItemsListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.item_list_view_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.items_list_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.item_list_nav_view);
        MainNavigationDrawerItemSelectedListener drawerItemSelectedListener =
                new MainNavigationDrawerItemSelectedListener(getApplicationContext(), this);
        navigationView.setNavigationItemSelectedListener(drawerItemSelectedListener);
        SystemUtil.setNavigationHeaderAndItemAccordingToUser(navigationView, this);

        String categoryId = getIntent().getStringExtra("categoryId");

        List<ItemListElement> itemList = DataLoaderService.loadItemListForCategory(categoryId);
        RecycleViewAdapter recycleViewAdapter = new RecycleViewAdapter(getApplicationContext(),
                itemList);
        RecyclerView myListView = (RecyclerView) findViewById(R.id.all_items_list);

        myListView.setAdapter(recycleViewAdapter);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        myListView.setLayoutManager(llm);

        myListView.addOnScrollListener(new ItemListCustomOnScrollListener(getBaseContext(),
                itemList, categoryId, recycleViewAdapter, myListView));

        String category = getIntent().getStringExtra("categoryName");
        category = TextUtils.isEmpty(category) ? "" : ":" + category;
        setTitle(String.format(getString(R.string.title_activity_items_list_view), category));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.items_list_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.items_list_view, menu);
        return true;
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        NavigationView navigationView = (NavigationView) findViewById(R.id.item_list_nav_view);
        SystemUtil.setNavigationHeaderAndItemAccordingToUser(navigationView, this);
    }

}
