package com.hsi.shoppingcart.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.response.LoginResponse;
import com.hsi.shoppingcart.util.SystemUtil;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A login screen that offers login via username/passwordTextView.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_INTERNET = 0;
    private static final String USER_LOGIN_URL = "http://shopping.api.localhost:9091/user/login";
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask userLoginTask = null;
    // UI references.
    private EditText userNameTextView;
    private EditText passwordTextView;
    private View progressView;
    private View loginForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setupActionBar();
        // Set up the login form.
        userNameTextView = (EditText) findViewById(R.id.username);
        passwordTextView = (EditText) findViewById(R.id.password);
        passwordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    executeLogin();
                    return true;
                }
                return false;
            }
        });

        Button loginButton = (Button) findViewById(R.id.btn_register_user_login);
        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                executeLogin();
            }
        });

        SystemUtil.requestInternetPermission(getApplicationContext(), this, REQUEST_INTERNET,
                userNameTextView);
        loginForm = findViewById(R.id.login_form);
        progressView = findViewById(R.id.login_progress);
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void executeLogin() {
        if (userLoginTask != null) {
            return;
        }

        // Reset errors.
        userNameTextView.setError(null);
        passwordTextView.setError(null);

        // Store values at the time of the login attempt.
        String userName = this.userNameTextView.getText().toString();
        String password = this.passwordTextView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid passwordTextView, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            this.passwordTextView.setError(getString(R.string.login_message_error_invalid_password));
            focusView = this.passwordTextView;
            cancel = true;
        }

        // Check for a valid userNameTextView address.
        if (TextUtils.isEmpty(userName)) {
            this.userNameTextView.setError(getString(R.string.login_message_error_field_required));
            focusView = this.userNameTextView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            userLoginTask = new UserLoginTask(userName, password);
            userLoginTask.execute();
        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 1;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
            loginForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_INTERNET && SystemUtil.isPermissionGranted(grantResults)) {
            Toast.makeText(this, R.string.login_internet_permission_granted_message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showRegistration(View view) {
        Intent intent = new Intent(getApplicationContext(), RegisterUserActivity.class);
        startActivity(intent);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String, Void, LoginResponse> {

        private final String userName;
        private final String password;

        UserLoginTask(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        @Override
        protected LoginResponse doInBackground(String... params) {
            try {
                URL url = new URL(USER_LOGIN_URL);

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("username", userName);
                postDataParams.put("password", password);
                Log.i("params", postDataParams.toString());

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setReadTimeout(15000 /* milliseconds */);
                connection.setConnectTimeout(15000 /* milliseconds */);
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(postDataParams.toString());
                writer.flush();
                writer.close();

                Gson gson = new Gson();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return gson.fromJson(reader, LoginResponse.class);

            } catch (Exception e) {
                Log.e(LoginActivity.class.getName(), "Error occurred", e);
                return LoginResponse.generateFailureLoginResponse();
            }
        }

        @Override
        protected void onPostExecute(final LoginResponse response) {
            userLoginTask = null;
            showProgress(false);
            if (response.isAuthenticated()) {
                SystemUtil.executeLogin(getApplicationContext(), response.getUserName());
                finish();
            } else {
                LoginActivity.this.passwordTextView.setError(getString(R.string.login_message_error_incorrect_credentials));
                LoginActivity.this.userNameTextView.setError(getString(R.string.login_message_error_incorrect_credentials));
                LoginActivity.this.passwordTextView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            userLoginTask = null;
            showProgress(false);
        }
    }
}

