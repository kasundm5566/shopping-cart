package com.hsi.shoppingcart.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.domain.RegisterUser;
import com.hsi.shoppingcart.services.UserRegistrationTask;
import com.hsi.shoppingcart.util.SystemUtil;

public class RegisterUserActivity extends AppCompatActivity {

    private static final int REQUEST_INTERNET = 0;

    private EditText userNameTextView;
    private EditText passwordTextView;
    private EditText emailTextView;
    private EditText addressTextView;

    private View registerView;
    private View progressView;

    private UserRegistrationTask registrationTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
        setupActionBar();

        userNameTextView = (EditText) findViewById(R.id.username);
        passwordTextView = (EditText) findViewById(R.id.password);
        emailTextView = (EditText) findViewById(R.id.email);
        addressTextView = (EditText) findViewById(R.id.address);

        registerView = findViewById(R.id.user_registration_form);
        progressView = findViewById(R.id.user_registration_progress);
        SystemUtil.requestInternetPermission(getApplicationContext(), this, REQUEST_INTERNET,
                userNameTextView);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void executeRegister(View view) {
        userNameTextView.setError(null);
        passwordTextView.setError(null);
        emailTextView.setError(null);
        addressTextView.setError(null);

        String userName = this.userNameTextView.getText().toString();
        String password = this.passwordTextView.getText().toString();
        String email = this.emailTextView.getText().toString();
        String address = this.addressTextView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            this.passwordTextView.setError(getString(R.string.register_message_error_invalid_password));
            focusView = this.passwordTextView;
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            this.emailTextView.setError(getString(R.string.register_message_error_invalid_email));
            focusView = this.emailTextView;
            cancel = true;
        }
        if (TextUtils.isEmpty(userName)) {
            this.userNameTextView.setError(getString(R.string.register_message_error_field_required));
            focusView = this.userNameTextView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            registrationTask = new UserRegistrationTask(this, progressView, registerView);
            registrationTask.showProgress(true);
            RegisterUser user = new RegisterUser();
            user.setUsername(userName);
            user.setPassword(password);
            user.setEmail(email);
            user.setAddress(address);
            registrationTask.execute(user);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_INTERNET &&
                SystemUtil.isPermissionGranted(grantResults)) {
            Toast.makeText(this, R.string.register_internet_permission_granted_message, Toast.LENGTH_SHORT).show();
        }
    }
}
