package com.hsi.shoppingcart.activities;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.adapters.ShoppingCartItemListAdapter;
import com.hsi.shoppingcart.services.ShoppingCartService;
import com.hsi.shoppingcart.domain.ShoppingCartProduct;
import com.hsi.shoppingcart.domain.ShoppingCartRealmModule;
import com.hsi.shoppingcart.services.DataLoaderService;
import com.hsi.shoppingcart.services.listeners.MainNavigationDrawerItemSelectedListener;
import com.hsi.shoppingcart.util.SystemUtil;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class ShoppingCartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        setTitle("Shopping cart");

        Toolbar toolbar = (Toolbar) findViewById(R.id.shopping_cart_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.items_list_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.item_list_nav_view);
        MainNavigationDrawerItemSelectedListener drawerItemSelectedListener =
                new MainNavigationDrawerItemSelectedListener(getApplicationContext(), this);
        navigationView.setNavigationItemSelectedListener(drawerItemSelectedListener);
        SystemUtil.setNavigationHeaderAndItemAccordingToUser(navigationView, this);

        //TODO: Remove deleteRealmIfMigrationNeeded() before a release.
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(getApplicationContext()).deleteRealmIfMigrationNeeded().setModules(new ShoppingCartRealmModule()).build();
        Realm.setDefaultConfiguration(realmConfiguration);

        double shoppingCartTotal = shoppingCartAllItemsTotal();
        TextView txtCartTotal = (TextView) findViewById(R.id.txtHeaderTotal);
        txtCartTotal.setText("Total : $" + String.format("%.2f", shoppingCartTotal));
        Button btnCheckout = (Button) findViewById(R.id.btnCheckout);
        TextView txtNoItemsMessage=(TextView)findViewById(R.id.txtNoItemsMessage);

        if (shoppingCartTotal > 0) {
            btnCheckout.setVisibility(View.VISIBLE);
            txtNoItemsMessage.setVisibility(View.GONE);
        } else {
            btnCheckout.setVisibility(View.GONE);
            txtNoItemsMessage.setText("There is no items in the shopping cart.");
            txtNoItemsMessage.setVisibility(View.VISIBLE);
        }

        ListAdapter listAdapter = new ShoppingCartItemListAdapter(ShoppingCartActivity.this,
                DataLoaderService.loadShoppingCartItems(ShoppingCartActivity.this));
        ListView myListView = (ListView) findViewById(R.id.cartItemsList);
        myListView.setAdapter(listAdapter);
    }

    private double shoppingCartAllItemsTotal() {
        ShoppingCartService shoppingCartService = new ShoppingCartService(getApplicationContext());
        List<ShoppingCartProduct> shoppingCartProducts = shoppingCartService.retrieveCart();
        double total = 0;
        if (shoppingCartProducts.size() > 0) {
            for (int i = 0; i < shoppingCartProducts.size(); i++) {
                ShoppingCartProduct shoppingCartProduct = shoppingCartProducts.get(i);
                total += shoppingCartProduct.getTotal();
            }
        }
        return total;
    }
}
