package com.hsi.shoppingcart.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.hsi.shoppingcart.R;

public class VideoPlayActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_play);
        Bundle extras = getIntent().getExtras();
        if (extras != null && !extras.isEmpty()) {
            String videoUrl = extras.getString("video_url");
            final ProgressDialog progressDialog = new ProgressDialog(VideoPlayActivity.this);
            progressDialog.setTitle("Video Category");
            progressDialog.setMessage("Loading the Video");
            progressDialog.show();

            final VideoView videoview = (VideoView) findViewById(R.id.video_preview_view);

            final MediaController mediacontroller = new MediaController(
                    VideoPlayActivity.this);
            mediacontroller.setAnchorView(videoview);
            try {
                // Get the URL from String VideoURL
                Uri video = Uri.parse(videoUrl);
                videoview.setMediaController(mediacontroller);
                videoview.setVideoURI(video);
            } catch (Exception e) {
                Log.e(VideoPlayActivity.class.getName(), "Error", e);
            }

            videoview.requestFocus();
            videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    progressDialog.dismiss();
                    videoview.start();
                    mediacontroller.show();
                }
            });
            videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    VideoPlayActivity.this.finish();
                }
            });
        }
    }
}
