package com.hsi.shoppingcart.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.activities.ItemDetailsActivity;
import com.hsi.shoppingcart.domain.ItemDetails;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.services.DataLoaderService;
import com.hsi.shoppingcart.services.listeners.ItemClickListener;

import java.util.List;


public class AllItemViewListAdapter extends ArrayAdapter<ItemListElement> {

    private Context context;

    public AllItemViewListAdapter(@NonNull Context context, List<ItemListElement> itemList) {
        super(context, R.layout.all_items_list_view_custom_row, itemList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.all_items_list_view_custom_row, parent, false);
        final ItemListElement item = getItem(position);

        TextView itemNameText = (TextView) customView.findViewById(R.id.item_name);
        TextView itemPriceText = (TextView) customView.findViewById(R.id.item_price);
        ImageView imageView = (ImageView) customView.findViewById(R.id.imageView);

        itemNameText.setText(item.getItemName());
        itemPriceText.setText(String.format("%.2f", item.getUnitPrice()));
        imageView.setImageResource(R.drawable.ic_menu_camera);

        customView.setOnClickListener(new ItemClickListener(item, context));
        return customView;
    }
}
