package com.hsi.shoppingcart.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.services.listeners.ItemClickListener;

import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ListItemViewHolder> {
    private List<ItemListElement> itemList;
    private Context currentContext;


    public RecycleViewAdapter(@NonNull Context currentContext, List<ItemListElement> itemList) {
        this.itemList = itemList;
        this.currentContext = currentContext;
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View customRowView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_items_list_view_custom_row, parent, false);
        return new ListItemViewHolder(customRowView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        ItemListElement item = itemList.get(position);
        holder.setItemNameText(item.getItemName());
        holder.setItemPriceText(item.getUnitPrice());
        holder.setItemImageResource(R.mipmap.ic_launcher_round);
        holder.setItemOnClickListener(currentContext, item);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    class ListItemViewHolder extends RecyclerView.ViewHolder {
        private TextView itemNameText;
        private TextView itemPriceText;
        private ImageView itemImageView;
        private View customRowView;


        public ListItemViewHolder(View customRowView) {
            super(customRowView);
            this.customRowView = customRowView;
            this.itemNameText = (TextView) customRowView.findViewById(R.id.item_name);
            this.itemPriceText = (TextView) customRowView.findViewById(R.id.item_price);
            this.itemImageView = (ImageView) customRowView.findViewById(R.id.imageView);
        }

        public void setItemNameText(String itemName) {
            this.itemNameText.setText(itemName);
        }

        public void setItemPriceText(double itemPrice) {
            this.itemPriceText.setText(String.format("%.2f", itemPrice));
        }

        public void setItemImageResource(int imageResourceId) {
            this.itemImageView.setImageResource(imageResourceId);
        }

        public void setItemOnClickListener(Context context, ItemListElement itemListElement) {
            customRowView.setOnClickListener(new ItemClickListener(itemListElement, context));
        }
    }


}