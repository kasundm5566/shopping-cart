package com.hsi.shoppingcart.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.activities.ItemDetailsActivity;
import com.hsi.shoppingcart.activities.ShoppingCartActivity;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.domain.ShoppingCartProduct;
import com.hsi.shoppingcart.services.ShoppingCartService;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by hsenid on 9/7/17.
 */

public class ShoppingCartItemListAdapter extends ArrayAdapter<ShoppingCartProduct> {

    Context context;

    public ShoppingCartItemListAdapter(@NonNull Context context, List<ShoppingCartProduct> items) {
        super(context, R.layout.shopping_cart_item, items);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.shopping_cart_item, parent, false);
        final ShoppingCartProduct item = getItem(position);

        TextView itemNameText = (TextView) customView.findViewById(R.id.txtItemName);
        TextView itemPriceText = (TextView) customView.findViewById(R.id.txtItemPrice);
        TextView itemQuanity = (TextView) customView.findViewById(R.id.txtQuantity);
        TextView itemTotal = (TextView) customView.findViewById(R.id.txtTotal);
        ImageView imageView = (ImageView) customView.findViewById(R.id.imgView);

        //TODO: API request to get the item name should be called from here.
        String itemName = "Item name of product " + item.getProductId();
        itemNameText.setText(itemName);
        itemQuanity.setText("Qty: " + item.getQuantity());
        itemPriceText.setText("Price: " + String.format("%.2f", item.getPrice()));
        itemTotal.setText("Total: " + String.format("%.2f", item.getTotal()));
        imageView.setImageResource(R.drawable.category_womens_clothes);

        ImageView imgRemove = (ImageView) customView.findViewById(R.id.imgRemove);
        imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.cart_remove_confirm_dialog);
                dialog.setTitle("Delete item?");
                Button btnRemove = (Button) dialog.findViewById(R.id.btnRemove);
                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                dialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        ShoppingCartService shoppingCartService = new ShoppingCartService(context);
                        shoppingCartService.removeFromCart(item);
                        Toast toast = Toast.makeText(context, "Item removed from the cart.", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        });

        ImageView imgUpdate = (ImageView) customView.findViewById(R.id.imgUpdate);
        imgUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.cart_add_dialog);
                dialog.setTitle("Change quantity");
                final EditText txtQuantity = (EditText) dialog.findViewById(R.id.txtQuantity);
                txtQuantity.setText(String.valueOf(item.getQuantity()));
                Button btnChangeQty = (Button) dialog.findViewById(R.id.btnAddToCart);
                btnChangeQty.setText("Update");
                Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                dialog.show();

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                btnChangeQty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (txtQuantity.getText().length() == 0 || Integer.parseInt(txtQuantity.getText().toString()) <= 0) {
                            txtQuantity.setError("Invalid quantity.");
                        } else {
                            ShoppingCartService shoppingCartService = new ShoppingCartService(context);
                            shoppingCartService.updateQuantity(item, Integer.parseInt(txtQuantity.getText().toString()));
                            dialog.dismiss();
                            Toast toast = Toast.makeText(context, "Item quantity updated.", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                });
            }
        });

        return customView;
    }
}
