package com.hsi.shoppingcart.domain;

import java.io.Serializable;

/**
 * Created by hsenid on 9/4/17.
 */

public class ItemDetails implements Serializable {
    private String itemId;
    private String name;
    private String description;
    private double price;
    private String videoUrl;
    private boolean isVideoItem = false;
    private boolean isYoutubeVideo = false;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public boolean isVideoItem() {
        return isVideoItem;
    }

    public void setVideoItem(boolean isVideoItem) {
        this.isVideoItem = isVideoItem;
    }

    public boolean isYoutubeVideo() {
        return isYoutubeVideo;
    }

    public void setYoutubeVideo(boolean isYoutubeVideo) {
        this.isYoutubeVideo = isYoutubeVideo;
    }
}
