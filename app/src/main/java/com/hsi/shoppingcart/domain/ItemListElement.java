package com.hsi.shoppingcart.domain;


public class ItemListElement {
    private String itemId;
    private String itemName;
    private double unitPrice;
    private String imageId;

    public ItemListElement() {
        // add no-arg constructor
    }

    public ItemListElement(String itemName, double unitPrice) {
        this(null,itemName,unitPrice,null);
    }

    public ItemListElement(String itemId, String itemName, double unitPrice, String imageId) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.unitPrice = unitPrice;
        this.imageId = imageId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    @Override
    public String toString() {
        return "ItemListElement{" +
                "itemId='" + itemId + '\'' +
                ", itemName='" + itemName + '\'' +
                ", unitPrice=" + unitPrice +
                ", imageId='" + imageId + '\'' +
                '}';
    }
}
