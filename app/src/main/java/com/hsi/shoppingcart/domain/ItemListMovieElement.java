package com.hsi.shoppingcart.domain;

/**
 * Created by vimukthi on 9/13/17.
 */

public class ItemListMovieElement extends ItemListElement {
    private String videoUrl;
    private boolean isYoutubeVideo;

    public ItemListMovieElement(String itemId,String itemName, double unitPrice, String videoUrl,
                                boolean isYoutubeVideo) {
        super(itemId,itemName, unitPrice,null);
        this.videoUrl = videoUrl;
        this.isYoutubeVideo = isYoutubeVideo;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public boolean isYoutubeVideo() {
        return isYoutubeVideo;
    }

    public void setYoutubeVideo(boolean youtubeVideo) {
        isYoutubeVideo = youtubeVideo;
    }
}
