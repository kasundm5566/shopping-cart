package com.hsi.shoppingcart.domain;

import java.util.List;

public class SearchResults<T> {
    private List<T> searchResultList;

    public SearchResults(List<T> searchResultList) {
        this.searchResultList = searchResultList;
    }

    public List<T> getSearchResultList() {
        return searchResultList;
    }
}
