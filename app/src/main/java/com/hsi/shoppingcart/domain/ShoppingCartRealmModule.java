package com.hsi.shoppingcart.domain;

import io.realm.annotations.RealmModule;

/**
 * Created by hsenid on 9/6/17.
 */

@RealmModule(classes = {ShoppingCartProduct.class})
public class ShoppingCartRealmModule {
}
