package com.hsi.shoppingcart.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.services.listeners.AllCategoryViewOnCLickListener;


public class AllCategoriesFragment extends Fragment {

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static AllCategoriesFragment newInstance() {
        return new AllCategoriesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_all_categories, container, false);
        addClickListenersForCategoryFragmentElements(rootView);
        return rootView;
    }

    private void addClickListenersForCategoryFragmentElements(View view) {
        addOnClickListener(view, R.id.mobile_phones_category_card_view, "cat001", getString(R.string.mobile_category_item_list_title));
        addOnClickListener(view, R.id.women_cloths_category_card_view, "cat002", getString(R.string.women_cloth_category_item_list_title));
        addOnClickListener(view, R.id.men_cloths_category_card_view, "cat003", getString(R.string.men_cloth_category_item_list_title));
        addOnClickListener(view, R.id.watches_category_card_view, "cat004", getString(R.string.watches_category_item_list_title));
        addOnClickListener(view, R.id.shoes_category_card_view, "cat005", getString(R.string.shoes_category_item_list_title));
        addOnClickListener(view, R.id.camera_category_card_view, "cat006", getString(R.string.cameras_category_item_list_title));
        addOnClickListener(view, R.id.tv_category_card_view, "cat007", getString(R.string.tv_category_item_list_title));
        addOnClickListener(view, R.id.music_category_card_view, "cat008", getString(R.string.music_category_item_list_title));
        addOnClickListener(view, R.id.video_category_card_view, "cat009", getString(R.string.video_category_item_list_title));
        addOnClickListener(view, R.id.jewelry_category_card_view, "cat010", getString(R.string.jewelry_category_item_list_title));
    }


    private void addOnClickListener(View view, int id, String categoryId, String categoryName) {
        CardView mobileCategory = (CardView) view.findViewById(id);
        mobileCategory.setOnClickListener(
                new AllCategoryViewOnCLickListener(categoryId, categoryName, this.getActivity()));
    }
}
