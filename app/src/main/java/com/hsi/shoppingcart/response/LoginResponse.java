package com.hsi.shoppingcart.response;


public class LoginResponse {
    private String userName;
    private boolean authenticated;
    private static final LoginResponse FAILURE_RESPONSE = new LoginResponse();

    public static LoginResponse generateFailureLoginResponse(){
        FAILURE_RESPONSE.setAuthenticated(false);
        return FAILURE_RESPONSE;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "userName='" + userName + '\'' +
                ", authenticated=" + authenticated +
                '}';
    }
}
