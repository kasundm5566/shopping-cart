package com.hsi.shoppingcart.response;


public class UserRegistrationResponse {
    private static final UserRegistrationResponse FAILURE_RESPONSE = new UserRegistrationResponse();
    private String username;
    private boolean authenticated;
    private boolean registrationSuccess;

    public static UserRegistrationResponse generateFailureResponse() {
        FAILURE_RESPONSE.setAuthenticated(false);
        FAILURE_RESPONSE.setRegistrationSuccess(false);
        return FAILURE_RESPONSE;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isRegistrationSuccess() {
        return registrationSuccess;
    }

    public void setRegistrationSuccess(boolean registrationSuccess) {
        this.registrationSuccess = registrationSuccess;
    }

    @Override
    public String toString() {
        return "UserRegistrationResponse{" +
                "username='" + username + '\'' +
                ", authenticated=" + authenticated +
                ", registrationSuccess=" + registrationSuccess +
                '}';
    }
}
