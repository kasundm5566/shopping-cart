package com.hsi.shoppingcart.services;


import android.content.Context;
import android.support.annotation.NonNull;

import com.hsi.shoppingcart.domain.ItemDetails;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.domain.ItemListMovieElement;
import com.hsi.shoppingcart.domain.ShoppingCartProduct;

import java.util.LinkedList;
import java.util.List;

public class DataLoaderService {

    public static List<ItemListElement> loadItemListForCategory(String categoryId) {
        // TODO: 9/4/17 implement the data loader to support paging and fetch the data from the api
        if ("cat009".equalsIgnoreCase(categoryId)) {
            return createVideoItemList(categoryId);
        } else {
            return createDefaultItemList(categoryId);
        }
    }

    private static List<ItemListElement> createVideoItemList(String categoryId) {
        List<ItemListElement> videoItems = new LinkedList<>();
        videoItems.add(new ItemListMovieElement(categoryId, "test item 1 (video from a server) of :" + categoryId, 20.5,
                "http://www.androidbegin.com/tutorial/AndroidCommercial.3gp", false));
        videoItems.add(new ItemListMovieElement(categoryId, "test item 2 (youtube video) of :" + categoryId, 20.5,
                "cdgQpa1pUUE", true));
        return videoItems;
    }

    @NonNull
    private static List<ItemListElement> createDefaultItemList(String categoryId) {
        List<ItemListElement> itemsList = new LinkedList<>();
        itemsList.add(new ItemListElement("test item 1 of :" + categoryId, 20.5));
        itemsList.add(new ItemListElement("test item 2 of :" + categoryId, 230.5));
        itemsList.add(new ItemListElement("test item 3 of :" + categoryId, 2130.5));
        itemsList.add(new ItemListElement("test item 4 of :" + categoryId, 1320.5));
        itemsList.add(new ItemListElement("test item 5 of :" + categoryId, 320.5));
        itemsList.add(new ItemListElement("test item 6 of :" + categoryId, 240.5));
        itemsList.add(new ItemListElement("test item 7 of :" + categoryId, 1331));
        itemsList.add(new ItemListElement("test item 8 of :" + categoryId, 315));
        itemsList.add(new ItemListElement("test item 9 of :" + categoryId, 13415));
        itemsList.add(new ItemListElement("test item 10 of :" + categoryId, 53));


        return itemsList;
    }

    public static ItemDetails retrieveItemDetails(String itemId) {
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItemId(itemId);
        itemDetails.setName("Sample item");
        itemDetails.setDescription("This is a sample description about the item");
        itemDetails.setPrice(12);
        return itemDetails;
    }

    /**
     * Function to load shopping cart items
     *
     * @param context Application context
     * @return List of shopping cart items (ShoppingCartProduct objects)
     */
    public static List<ShoppingCartProduct> loadShoppingCartItems(Context context) {
        // TODO: 9/4/17 implement the data loader to support paging and fetch the data from the api
        ShoppingCartService shoppingCartService = new ShoppingCartService(context);
        List<ShoppingCartProduct> shoppingCartProducts = shoppingCartService.retrieveCart();

        return shoppingCartProducts;
    }
}
