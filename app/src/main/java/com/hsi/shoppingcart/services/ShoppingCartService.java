package com.hsi.shoppingcart.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.hsi.shoppingcart.domain.ShoppingCartProduct;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by hsenid on 9/6/17.
 */

public class ShoppingCartService {

    private String username;

    public ShoppingCartService(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.username = sharedPreferences.getString("username", "guest");
    }

    /**
     * Function to add items to the shopping cart. Currently the shopping cart data is saved in a Realm database.
     *
     * @param productId Id of the product which needs to be added to the cart
     * @param quantity  Number of items to be purchased
     * @param price     Price of the item
     */
    public void addToCart(String productId, int quantity, double price) {
        Realm realm = Realm.getDefaultInstance();
        try {
            realm.beginTransaction();
            ShoppingCartProduct shoppingCartProduct = realm.createObject(ShoppingCartProduct.class);
            // Increment the primary key value
            Number currentId = realm.where(ShoppingCartProduct.class).max("id");
            int nextId;
            if (currentId == null) {
                nextId = 1;
            } else {
                nextId = currentId.intValue() + 1;
            }
            shoppingCartProduct.setId(nextId);
            shoppingCartProduct.setUsername(username);
            shoppingCartProduct.setProductId(productId);
            shoppingCartProduct.setStatus("in-cart");
            shoppingCartProduct.setQuantity(quantity);
            shoppingCartProduct.setPrice(price);
            shoppingCartProduct.setTotal(price * quantity);
            realm.commitTransaction();
        } finally {
            realm.close();
        }
    }

    /**
     * Function to retrieve shopping cart items
     *
     * @return List of shopping cart items (ShoppingCartProduct objects)
     */
    public List<ShoppingCartProduct> retrieveCart() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery realmQuery = realm.where(ShoppingCartProduct.class).equalTo("status", "in-cart").equalTo("username", username);
        RealmResults realmResults = realmQuery.findAll();
        List<ShoppingCartProduct> shoppingCartProductList = new ArrayList<>();
        for (int i = 0; i < realmResults.size(); i++) {
            shoppingCartProductList.add((ShoppingCartProduct) realmResults.get(i));
        }
        realm.close();
        return shoppingCartProductList;
    }

    /**
     * Function to remove an item/product from the shopping cart
     *
     * @param shoppingCartProduct Shopping cart product to be removed from the shopping cart
     */
    public void removeFromCart(ShoppingCartProduct shoppingCartProduct) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery realmQuery = realm.where(ShoppingCartProduct.class).equalTo("status", "in-cart").equalTo("id", shoppingCartProduct.getId());
        realm.beginTransaction();
        RealmResults realmResults = realmQuery.findAll();
        realmResults.clear();
        realm.commitTransaction();
        realm.close();
    }

    /**
     * Function to update the quantity of a product in a shopping cart
     *
     * @param shoppingCartProduct Shopping cart product to be updated
     * @param quantity            New quantity amount
     */
    public void updateQuantity(ShoppingCartProduct shoppingCartProduct, int quantity) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery realmQuery = realm.where(ShoppingCartProduct.class).equalTo("status", "in-cart").equalTo("id", shoppingCartProduct.getId());
        ShoppingCartProduct product = (ShoppingCartProduct) realmQuery.findFirst();
        realm.beginTransaction();
        product.setQuantity(quantity);
        product.setTotal(product.getPrice() * quantity);
        realm.commitTransaction();
        realm.close();
    }
}
