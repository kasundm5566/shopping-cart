package com.hsi.shoppingcart.services;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.hsi.shoppingcart.domain.RegisterUser;
import com.hsi.shoppingcart.response.UserRegistrationResponse;
import com.hsi.shoppingcart.util.SystemUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class UserRegistrationTask extends AsyncTask<RegisterUser, Void, UserRegistrationResponse> {

    private static final String USER_LOGIN_URL = "http://shopping.api.localhost:9091/user/register";
    private Activity currentActivity;
    private View progressView;
    private View registrationForm;

    public UserRegistrationTask(Activity parentActivity, View progressView, View registrationForm) {
        this.currentActivity = parentActivity;
        this.progressView = progressView;
        this.registrationForm = registrationForm;
    }

    @Override
    protected UserRegistrationResponse doInBackground(RegisterUser... registerUsers) {
        RegisterUser user = registerUsers[0];
        Gson gson = new Gson();
        try {
            URL url = new URL(USER_LOGIN_URL);

            Log.i("params", user.toString());

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setReadTimeout(15000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(gson.toJson(user));
            writer.flush();
            writer.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            return gson.fromJson(reader, UserRegistrationResponse.class);
        } catch (IOException e) {
            Log.e(UserRegistrationTask.class.getName(), "Error occurred", e);
            return UserRegistrationResponse.generateFailureResponse();
        }
    }

    @Override
    protected void onPostExecute(UserRegistrationResponse response) {
        showProgress(false);
        if (response.isRegistrationSuccess() && response.isAuthenticated()) {
            SystemUtil.executeLogin(currentActivity.getApplicationContext(), response.getUsername());
            currentActivity.finish();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = currentActivity.getResources().getInteger(android.R.integer.config_shortAnimTime);

            registrationForm.setVisibility(show ? View.GONE : View.VISIBLE);
            registrationForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    registrationForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            registrationForm.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


}
