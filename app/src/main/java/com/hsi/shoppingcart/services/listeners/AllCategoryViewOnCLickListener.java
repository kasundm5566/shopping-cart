package com.hsi.shoppingcart.services.listeners;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.hsi.shoppingcart.activities.ItemsListViewActivity;


public class AllCategoryViewOnCLickListener implements View.OnClickListener {

    private String categoryId;
    private String categoryName;
    private Activity currentActivity;

    public AllCategoryViewOnCLickListener(String categoryId, String categoryName, Activity currentActivity) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.currentActivity = currentActivity;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(currentActivity.getApplicationContext(), ItemsListViewActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("categoryName", categoryName);
        currentActivity.startActivity(intent);

    }
}
