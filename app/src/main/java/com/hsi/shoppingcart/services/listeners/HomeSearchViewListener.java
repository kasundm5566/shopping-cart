package com.hsi.shoppingcart.services.listeners;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.ListView;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.adapters.AllItemViewListAdapter;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.domain.SearchResults;
import com.hsi.shoppingcart.util.SystemUtil;


public class HomeSearchViewListener implements SearchView.OnQueryTextListener {

    private Context currentContext;
    private Activity currentActivity;

    public HomeSearchViewListener(Context currentContext, Activity currentActivity) {
        this.currentContext = currentContext;
        this.currentActivity = currentActivity;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        SystemUtil.executeSearch(query, currentContext);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        SearchResults<ItemListElement> searchResults = SystemUtil.executeSearch(newText, currentContext);
        AllItemViewListAdapter listAdapter = new AllItemViewListAdapter(currentContext, searchResults.getSearchResultList());
        AppBarLayout appBarLayout = (AppBarLayout) currentActivity.findViewById(R.id.appbar);
        ViewPager viewPager = (ViewPager) currentActivity.findViewById(R.id.container);
        ListView listView = (ListView) currentActivity.findViewById(R.id.searchListView);
        listView.setAdapter(listAdapter);
        viewPager.setVisibility(View.INVISIBLE);
        appBarLayout.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.VISIBLE);

        return false;
    }
}
