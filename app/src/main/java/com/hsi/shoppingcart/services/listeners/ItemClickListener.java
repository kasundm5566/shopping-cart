package com.hsi.shoppingcart.services.listeners;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hsi.shoppingcart.activities.ItemDetailsActivity;
import com.hsi.shoppingcart.domain.ItemDetails;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.domain.ItemListMovieElement;
import com.hsi.shoppingcart.services.DataLoaderService;

/**
 * Created by hsenid on 9/8/17.
 */

/**
 * Listener to fire for a click event of an item in a listview containing items of a category
 */
public class ItemClickListener implements View.OnClickListener {

    private ItemListElement item;
    private Context context;

    public ItemClickListener(ItemListElement item, Context context) {
        this.item = item;
        this.context = context;
    }

    @Override
    public void onClick(View v) {
        ItemDetails itemDetails = DataLoaderService.retrieveItemDetails(item.getItemId());
        if (this.item instanceof ItemListMovieElement) {
            itemDetails.setVideoUrl(((ItemListMovieElement) this.item).getVideoUrl());
            itemDetails.setVideoItem(true);
            itemDetails.setYoutubeVideo(((ItemListMovieElement) this.item).isYoutubeVideo());
        }
        Intent intent = new Intent(context, ItemDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("selectedItem", itemDetails);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
