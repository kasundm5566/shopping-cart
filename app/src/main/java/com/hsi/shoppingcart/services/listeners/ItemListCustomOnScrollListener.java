package com.hsi.shoppingcart.services.listeners;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.hsi.shoppingcart.adapters.RecycleViewAdapter;
import com.hsi.shoppingcart.domain.ItemListElement;
import com.hsi.shoppingcart.services.DataLoaderService;

import java.util.List;


public class ItemListCustomOnScrollListener extends RecyclerView.OnScrollListener {
    private static final int DEFAULT_VISIBLE_THRESHOLD = 5;
    private boolean isLoading = false;
    private int currentPage;
    private Context currentContext;
    private List<ItemListElement> currentItemList;
    private String itemCategoryId;
    private RecycleViewAdapter viewAdapter;
    private RecyclerView currentView;

    public ItemListCustomOnScrollListener(Context currentContext,
                                          List<ItemListElement> currentItemList,
                                          String itemCategoryId, RecycleViewAdapter viewAdapter,
                                          RecyclerView currentView) {
        this.currentContext = currentContext;
        this.currentItemList = currentItemList;
        this.itemCategoryId = itemCategoryId;
        this.viewAdapter = viewAdapter;
        this.currentView = currentView;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
        LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading && hasMoreDataToLoad(currentPage)
                && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= DEFAULT_VISIBLE_THRESHOLD) {
            currentPage++;
            loadMoreItems(currentPage);
        }
    }

    private void loadMoreItems(int page) {
        isLoading = true;
        if (hasMoreDataToLoad(page)) {
            Toast.makeText(currentContext, "Loading List Items for page " + (page + 1), Toast.LENGTH_SHORT).show();
            currentItemList.addAll(DataLoaderService.loadItemListForCategory(itemCategoryId));
            new Thread(new Runnable() {
                @Override
                public void run() {
                    currentView.post(new Runnable() {
                        @Override
                        public void run() {
                            viewAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }).start();

        }
        isLoading = false;
    }

    private boolean hasMoreDataToLoad(int page) {
        // TODO: 9/8/17 this should query from the api and check if there are more data after the current page
        return page + 1 <= 10;
    }
}
