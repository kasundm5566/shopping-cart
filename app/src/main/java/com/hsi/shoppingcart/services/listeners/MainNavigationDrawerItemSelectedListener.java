package com.hsi.shoppingcart.services.listeners;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.MenuItem;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.activities.LoginActivity;
import com.hsi.shoppingcart.activities.ShoppingCartActivity;
import com.hsi.shoppingcart.util.SystemUtil;


public class MainNavigationDrawerItemSelectedListener implements
        NavigationView.OnNavigationItemSelectedListener {

    private Context applicationContext;
    private Activity currentActivity;

    public MainNavigationDrawerItemSelectedListener(Context applicationContext, Activity currentActivity) {
        this.applicationContext = applicationContext;
        this.currentActivity = currentActivity;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_goto_cart) {
            Intent intent = new Intent(applicationContext, ShoppingCartActivity.class);
            currentActivity.startActivity(intent);
        } else if (id == R.id.nav_account) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_logout) {
            SystemUtil.executeLogout(applicationContext);
            Intent intent = new Intent(applicationContext, LoginActivity.class);
            currentActivity.startActivity(intent);
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(applicationContext, LoginActivity.class);
            currentActivity.startActivity(intent);
        }
        return true;
    }
}
