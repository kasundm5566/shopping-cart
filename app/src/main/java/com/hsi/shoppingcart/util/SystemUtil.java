package com.hsi.shoppingcart.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hsi.shoppingcart.R;
import com.hsi.shoppingcart.domain.SearchResults;
import com.hsi.shoppingcart.services.DataLoaderService;

import static android.Manifest.permission.INTERNET;


public class SystemUtil {

    public static void executeLogin(Context context, String userName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("username", userName);
        editor.apply();
        editor.commit();
    }

    public static void executeLogout(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove("username");
        editor.apply();
        editor.commit();
    }

    public static String getLoggedInUser(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("username", "Guest");

    }

    public static void setNavigationHeaderAndItemAccordingToUser(NavigationView navigationView, Activity currentActivity) {
        TextView navHeaderUser = (TextView) navigationView.getHeaderView(0).findViewById(R.id.nav_user_name_txt);
        String loggedInUser = SystemUtil.getLoggedInUser(currentActivity.getApplicationContext());
        navHeaderUser.setText(String.format(currentActivity.getString(R.string.main_drawer_text_user), loggedInUser));
        MenuItem login = navigationView.getMenu().findItem(R.id.nav_login);
        MenuItem logout = navigationView.getMenu().findItem(R.id.nav_logout);
        if (!"guest".equalsIgnoreCase(loggedInUser)) {
            logout.setVisible(true);
            login.setVisible(false);
        } else {
            logout.setVisible(false);
            login.setVisible(true);
        }
    }

    public static boolean isPermissionGranted(@NonNull int[] grantResults) {
        return grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean requestInternetPermission(Context applicationContext,
                                                    final Activity currentActivity,
                                                    final int requestId,
                                                    View viewToFindParent) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (applicationContext.checkSelfPermission(INTERNET) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (currentActivity.shouldShowRequestPermissionRationale(INTERNET)) {
            Snackbar.make(viewToFindParent, R.string.register_internet_permission_request_message, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            currentActivity.requestPermissions(new String[]{INTERNET}, requestId);
                        }
                    });
        } else {
            currentActivity.requestPermissions(new String[]{INTERNET}, requestId);
        }
        return false;
    }


    public static SearchResults executeSearch(String query, Context context) {
        // TODO: 9/11/17 get the search results from api for the items
        return new SearchResults(DataLoaderService.loadItemListForCategory(query));
    }
}
